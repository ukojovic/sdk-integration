package com.integration.sdkintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients(basePackages = {"com.integration"})
public class SdkIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(SdkIntegrationApplication.class, args);
	}

}
