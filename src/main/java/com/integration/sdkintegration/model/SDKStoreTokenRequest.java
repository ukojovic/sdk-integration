package com.integration.sdkintegration.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class SDKStoreTokenRequest {

    private String sdkScopeToken;
}