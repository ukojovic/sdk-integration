package com.integration.sdkintegration.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class RefreshTokenRequest {

    private String currentRefreshToken;
}