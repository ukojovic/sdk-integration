package com.integration.sdkintegration.model;

import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "playlist_session")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString(onlyExplicitlyIncluded = true)
public class PlaylistSession {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "session_id")
    private UUID sessionId;

    @Column(name = "playlist_id")
    private String playlistId;

    @Column(name = "user_id")
    private String userId;
}
