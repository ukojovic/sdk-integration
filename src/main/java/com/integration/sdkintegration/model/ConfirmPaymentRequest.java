package com.integration.sdkintegration.model;

import com.integration.sdkintegration.model.dto.enumeration.BillingType;
import lombok.*;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class ConfirmPaymentRequest {

    @NonNull
    private UUID trxId;
    @NonNull
    private UUID appId;
    @NonNull
    private String payType;
    @NonNull
    private BillingType billingType;
    @NonNull
    private String subscriptionId;
    @NonNull
    private String userIp;
}
