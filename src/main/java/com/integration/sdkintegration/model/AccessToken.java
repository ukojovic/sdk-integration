package com.integration.sdkintegration.model;

import lombok.*;
import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "access_token")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString(onlyExplicitlyIncluded = true)
public class AccessToken {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "value", length = 2000)
    private String value;

    @Column(name = "expires_at")
    private Date expiresAt;

    @Column(name = "user_id")
    private String userId;

    @Column(name = "device_id")
    private String deviceId;
}
