package com.integration.sdkintegration.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class TokenResponse {

    private String accessToken;
    private String refreshToken;
}
