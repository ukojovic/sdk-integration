package com.integration.sdkintegration.model.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class PlaylistNextResponse {

    private String  trackUrl;
    private String expiresAt;
    private String imageUrl;
    private Long trackId;
}
