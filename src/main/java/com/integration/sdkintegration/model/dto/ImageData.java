package com.integration.sdkintegration.model.dto;

import lombok.*;

import java.net.URL;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class ImageData {

    private URL preview;
    private URL origin;

}
