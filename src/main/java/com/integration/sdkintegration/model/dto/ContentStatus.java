package com.integration.sdkintegration.model.dto;

public enum ContentStatus {
    AVAILABLE,
    PURCHASED,
    UNAVAILABLE,
}
