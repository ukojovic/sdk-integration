package com.integration.sdkintegration.model.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class Genre {

    private Long id;
    private String name;
}
