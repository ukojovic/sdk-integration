package com.integration.sdkintegration.model.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class PlaylistInfo {
    private String id;
    private String title;
    private String description;
    private Long trackCount;
    private String duration;
}
