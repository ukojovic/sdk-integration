package com.integration.sdkintegration.model.dto;

import lombok.*;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class PlaylistResponse {
    private List<PlaylistInfo> playlists;
}
