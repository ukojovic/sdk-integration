package com.integration.sdkintegration.model.dto;

import lombok.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class StyngPageItem {

    private UUID id;
    private String name;
    private String artist;
    private String album;
    private String duration;    //Duration
    private List<Genre> genres;
    private ImageData image;
    private BigDecimal price;
    private CurrencyInfo currency;
    private ContentStatus status;
    private Boolean isNew;
    private Boolean isPreviewAvailable;
}
