package com.integration.sdkintegration.model.dto;

import lombok.*;

import java.math.BigDecimal;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class ProductBuyResponse {

    private String productName;
    private String productDescription;
    private UUID transactionId;
    private String userId;
    private BigDecimal price;
    private String paymentUrl;
}
