package com.integration.sdkintegration.model.dto.enumeration;

public enum BillingType {
    STORE,
    BUNDLE,
    PROMO,
    MOBILE,
}
