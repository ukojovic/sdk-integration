package com.integration.sdkintegration.model.dto;

import lombok.*;

import java.net.URL;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class CurrencyInfo {

    private Long id;
    private String name;
    private URL imageUrl;
}
