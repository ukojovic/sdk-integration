package com.integration.sdkintegration.model.dto;

import lombok.*;

import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@EqualsAndHashCode
@ToString
public class PlaylistSessionResponse {

    private UUID sessionId;
}
