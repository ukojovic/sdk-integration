package com.integration.sdkintegration.service.feign;

import com.integration.sdkintegration.model.dto.ProductBuyResponse;
import com.integration.sdkintegration.model.dto.StyngPageResponse;
import com.integration.sdkintegration.model.dto.StyngsMyResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@FeignClient(name = "styngClient", url = "http://localhost:8080/styngs")
public interface StyngClient {

    @GetMapping
    StyngPageResponse get(@RequestParam(name = "name") String name,
                          @RequestParam(name = "size") Integer size,
                          @RequestParam(name = "page") Integer page);

    @GetMapping(path = "/my")
    StyngsMyResponse my();

    @PostMapping(path = "/{id}/buy")
    ProductBuyResponse buy(@PathVariable(name = "id") UUID id);
}
