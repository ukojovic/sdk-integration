package com.integration.sdkintegration.service.feign;

import com.integration.sdkintegration.model.ConfirmPaymentRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(name = "paymentsClient", url = "http://localhost:8080/payments")
public interface PaymentsClient {

    @PostMapping(path = "/confirm")
    void confirm(@RequestBody ConfirmPaymentRequest confirmPaymentRequest);
//    void confirm(@RequestHeader("x-api-token") String apiKey, @RequestBody ConfirmPaymentRequest confirmPaymentRequest);
}
