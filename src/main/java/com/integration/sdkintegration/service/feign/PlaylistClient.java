package com.integration.sdkintegration.service.feign;

import com.integration.sdkintegration.model.dto.PlaylistNextResponse;
import com.integration.sdkintegration.model.dto.PlaylistResponse;
import com.integration.sdkintegration.model.dto.PlaylistSessionBody;
import com.integration.sdkintegration.model.dto.PlaylistSessionResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(name = "playlistClient", url = "http://localhost:8080/playlists")
public interface PlaylistClient {

    @GetMapping
    PlaylistResponse playlists();

    @PostMapping(path = "/{id}/start")
    PlaylistSessionResponse startPlaylist(@PathVariable("id")String playlistId);

    @PostMapping(path = "/{id}/next")
    PlaylistNextResponse nextSong(@PathVariable("id")String playlistId, @RequestBody PlaylistSessionBody playlistSessionBody);

    @PostMapping(path = "/{id}/skip")
    PlaylistNextResponse skipSong(@PathVariable("id") String playlistId, @RequestBody PlaylistSessionBody playlistSessionBody);

    @PostMapping(path = "/{id}/resume")
    PlaylistNextResponse resume(@PathVariable("id") String playlistId, @RequestBody PlaylistSessionBody playlistSessionBody);
}
