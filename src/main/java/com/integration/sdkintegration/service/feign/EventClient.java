package com.integration.sdkintegration.service.feign;

import org.springframework.cloud.openfeign.FeignClient;

@FeignClient(name = "event", url = "http://localhost:8080/")
public interface EventClient {

}
