package com.integration.sdkintegration.service;

import com.integration.sdkintegration.model.AccessToken;
import com.integration.sdkintegration.model.RefreshToken;
import com.integration.sdkintegration.model.TokenResponse;
import com.integration.sdkintegration.service.dao.AuthDao;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;

@Service
public class AuthService {

    //    private final String STYNGR_SDK_BASE_PATH = "https://api.styngr/api/v1/sdk/";
    private final String STYNGR_SDK_BASE_PATH = "http://localhost:8080";
    private final String STYNGR_SDK_STORE_TOKEN_API = "/sdkuser";

    private final String STYNGR_SDK_TOKEN_API = "/tokens";
    private final String CONTENT_TYPE_HEADER_NAME = "Content-Type";
    private final String USER_ID = "a366e90f-c1a8-41fd-b4aa-a6969b75f676";
    private final String DEVICE_ID = "176e3f42-757c-461c-92bc-2190699f2fd8";
    private final AuthDao authDao;

    public AuthService(AuthDao authDao){
        this.authDao = authDao;
    }

    public Optional<String> obtainSDKScopeToken(){

        String x_API_TOKEN_HEADER_NAME = "x-api-token";
        HttpRequest tokenRequest = HttpRequest.newBuilder()
                .uri(URI.create(STYNGR_SDK_BASE_PATH + STYNGR_SDK_TOKEN_API))
                .POST(HttpRequest.BodyPublishers.ofString("{\n"
                        + "\"appId\": \"53b26ca6-5b48-4df2-bb38-0c5a4f1880f6\",\n"
                        +"\"deviceId\": " + "\"" + DEVICE_ID + "\",\n"
                        + "\"expiresIn\": \"PT24H\",\n" + "\"platform\": \"Linux\",\n"
                        + "\"userId\": " + "\"" + USER_ID + "\"\n"
                        + "}"))
                .header(x_API_TOKEN_HEADER_NAME, "akndM1JYB2ffPfdAQTC6R4LeCEc9RDa9")
                .header(CONTENT_TYPE_HEADER_NAME, "application/json")
                .build();

        HttpClient httpClient = HttpClient.newHttpClient();

        try {
            HttpResponse<String> response = httpClient.send(tokenRequest, HttpResponse.BodyHandlers.ofString());
            String body = Optional.of(response.body()).get();
            JSONObject jsonBody = new JSONObject(body);
            return Optional.of(jsonBody.get("token").toString());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return Optional.empty();
    }

    public Optional<TokenResponse> obtainSDKStoreToken(String sdkScopeToken) {

        String AUTHORIZATION_HEADER_NAME = "Authorization";
        HttpRequest tokenRequest = HttpRequest.newBuilder()
                .uri(URI.create(STYNGR_SDK_BASE_PATH + STYNGR_SDK_TOKEN_API + STYNGR_SDK_STORE_TOKEN_API))
                .POST(HttpRequest.BodyPublishers.noBody())
                .header(AUTHORIZATION_HEADER_NAME, "Bearer " + sdkScopeToken)
                .header(CONTENT_TYPE_HEADER_NAME, "application/json")
                .build();

        HttpClient httpClient = HttpClient.newHttpClient();

        try {
            HttpResponse<String> response = httpClient.send(tokenRequest, HttpResponse.BodyHandlers.ofString());
            String body = Optional.of(response.body()).get();
            JSONObject jsonBody = new JSONObject(body);
            AccessToken accessToken = parseAccessTokenFromJSON(jsonBody);
            AccessToken accessTokenSaved = authDao.saveAccessToken(accessToken);

            String refreshTokenValue = jsonBody.get("refreshToken").toString();
            RefreshToken refreshToken = RefreshToken.builder()
                    .userId(USER_ID)
                    .value(refreshTokenValue)
                    .deviceId(DEVICE_ID)
                    .build();
            RefreshToken savedRefreshToken = authDao.saveRefreshToken(refreshToken);

            return Optional.of(TokenResponse.builder()
                    .accessToken(accessTokenSaved.getValue())
                    .refreshToken(savedRefreshToken.getValue())
                    .build());

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return Optional.empty();
    }

    public Optional<TokenResponse> refreshToken(String currentRefreshToken) {

        String STYNGR_REFRESH_STORE_TOKEN_API = "/refresh";
        HttpRequest tokenRequest = HttpRequest.newBuilder()
                .uri(URI.create(STYNGR_SDK_BASE_PATH + STYNGR_SDK_TOKEN_API + STYNGR_SDK_STORE_TOKEN_API + STYNGR_REFRESH_STORE_TOKEN_API))
                .POST(HttpRequest.BodyPublishers.ofString("{\n" + " \"refreshToken\": \"" + currentRefreshToken
                        + "\"" + "}"))
                .header(CONTENT_TYPE_HEADER_NAME, "application/json")
                .build();

        HttpClient httpClient = HttpClient.newHttpClient();
        try {
            HttpResponse<String> response = httpClient.send(tokenRequest, HttpResponse.BodyHandlers.ofString());
            String body = Optional.of(response.body()).get();
            JSONObject jsonBody = new JSONObject(body);

            //TODO: if jsonBody doesn't have accessToken it means that refresh token expired and I should delete access and refresh token from db and request new one
            if(!jsonBody.has("accessToken")){

            }

            AccessToken accessToken = parseAccessTokenFromJSON(jsonBody);
            AccessToken existingAccessToken = authDao.findAccessTokenByUserId(USER_ID);
            existingAccessToken.setExpiresAt(accessToken.getExpiresAt());
            existingAccessToken.setValue(accessToken.getValue());
            AccessToken updatedAccessToken = authDao.saveAccessToken(existingAccessToken);

            String refreshTokenValue = jsonBody.get("refreshToken").toString();
            RefreshToken existingRefreshToken = authDao.findRefreshTokenByUserId(USER_ID);
            existingRefreshToken.setValue(refreshTokenValue);
            RefreshToken updatedRefreshToken = authDao.saveRefreshToken(existingRefreshToken);

            return Optional.of(TokenResponse.builder()
                    .accessToken(updatedAccessToken.getValue())
                    .refreshToken(updatedRefreshToken.getValue())
                    .build());

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        return Optional.empty();
    }

    private AccessToken parseAccessTokenFromJSON(JSONObject jsonBody) throws JSONException {

        String accessTokenValue = jsonBody.get("accessToken").toString();

        String[] accessChunks = accessTokenValue.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();
        String payloadAccess = new String(decoder.decode(accessChunks[1]));
        JSONObject jsonAccessTokenPayload = new JSONObject(payloadAccess);
        String expirationTime = jsonAccessTokenPayload.get("exp").toString();
        String deviceId = jsonAccessTokenPayload.get("device").toString();
        Date expirationDate = new Date(Integer.parseInt(expirationTime) * 1000L);

        return AccessToken.builder()
                .userId(USER_ID)
                .value(accessTokenValue)
                .expiresAt(expirationDate)
                .deviceId(deviceId)
                .build();
    }

}
