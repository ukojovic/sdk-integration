package com.integration.sdkintegration.service;

import feign.RequestInterceptor;
import feign.RequestTemplate;

public class AuthRequestInterceptor implements RequestInterceptor {

    private String token;
    private final String apiKeyHeaderName = "x-api-token";
    private final String apiKey = "akndM1JYB2ffPfdAQTC6R4LeCEc9RDa9";
    public AuthRequestInterceptor(String token){
        this.token = token;
    }

    public AuthRequestInterceptor(){}

    @Override
    public void apply(RequestTemplate requestTemplate) {
        if(token != null)
            requestTemplate.header("Authorization", "Bearer " + token);
        requestTemplate.header("Content-Type", "application/json");
        requestTemplate.header(apiKeyHeaderName, apiKey);
    }
}
