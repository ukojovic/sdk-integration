package com.integration.sdkintegration.service.dao;

import com.integration.sdkintegration.model.AccessToken;
import com.integration.sdkintegration.model.RefreshToken;
import com.integration.sdkintegration.repository.AccessTokenRepository;
import com.integration.sdkintegration.repository.RefreshTokenRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AuthDao {

    private AccessTokenRepository accessTokenRepository;
    private RefreshTokenRepository refreshTokenRepository;

    public AuthDao(AccessTokenRepository accessTokenRepository, RefreshTokenRepository refreshTokenRepository)
    {
        this.accessTokenRepository = accessTokenRepository;
        this.refreshTokenRepository = refreshTokenRepository;
    }

    public AccessToken saveAccessToken(AccessToken accessToken){
        log.info("Saving access token");
        return accessTokenRepository.save(accessToken);
    }

    public RefreshToken saveRefreshToken(RefreshToken refreshToken){
        log.info("Saving refresh token");
        return refreshTokenRepository.save(refreshToken);
    }

    public AccessToken findAccessTokenByUserId(String userId){
        log.info("Finding access token by user id " + userId);
        return accessTokenRepository.findByUserId(userId);
    }

    public RefreshToken findRefreshTokenByUserId(String userId){
        log.info("Finding refresh token by user id " + userId);
        return refreshTokenRepository.findByUserId(userId);
    }
}
