package com.integration.sdkintegration.service.dao;

import com.integration.sdkintegration.model.PlaylistSession;
import com.integration.sdkintegration.repository.PlaylistSessionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class PlaylistDao {

    private PlaylistSessionRepository playlistSessionRepository;

    public PlaylistDao(PlaylistSessionRepository playlistSessionRepository){
        this.playlistSessionRepository = playlistSessionRepository;
    }

    public PlaylistSession saveSession(PlaylistSession session){
        log.info("Saving session");
        return playlistSessionRepository.save(session);
    }

    public PlaylistSession findSessionByPlaylistIdAndUserId(String playlistId, String userId){
        log.info("Finding session by playlistId and userId");
        return playlistSessionRepository.findByPlaylistIdAndUserId(playlistId, userId);
    }
}
