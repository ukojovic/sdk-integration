package com.integration.sdkintegration.service;

import com.integration.sdkintegration.model.AccessToken;
import com.integration.sdkintegration.model.ConfirmPaymentRequest;
import com.integration.sdkintegration.model.dto.ProductBuyResponse;
import com.integration.sdkintegration.model.dto.StyngPageResponse;
import com.integration.sdkintegration.model.dto.StyngsMyResponse;
import com.integration.sdkintegration.model.dto.enumeration.BillingType;
import com.integration.sdkintegration.service.dao.AuthDao;
import com.integration.sdkintegration.service.feign.PaymentsClient;
import com.integration.sdkintegration.service.feign.StyngClient;
import feign.Feign;
import feign.FeignException;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Slf4j
@Service
public class StyngService {

    private AuthDao authDao;
        private final String USER_ID = "a366e90f-c1a8-41fd-b4aa-a6969b75f676"; //correct
        private final String API_KEY = "akndM1JYB2ffPfdAQTC6R4LeCEc9RDa9";


    public StyngService(AuthDao authDao){
        this.authDao = authDao;
    }
    public ResponseEntity<?> getAll(Integer pageSize, Integer pageIndex, String searchInput) {

        AccessToken accessToken = authDao.findAccessTokenByUserId(USER_ID);
        StyngClient client = initStyngClient(accessToken.getValue());

        try{
            StyngPageResponse response = client.get(searchInput, pageSize, pageIndex);
            return ResponseEntity.of(Optional.of(response));
        }catch(FeignException e){
            log.error("Exception while trying to get all styngs: " + e.getMessage());
            return ResponseEntity.of(Optional.of(e.getMessage()));
        }
    }
    public ResponseEntity<?> myStyngs() {

        AccessToken accessToken = authDao.findAccessTokenByUserId(USER_ID);
        if(accessToken == null){
            return ResponseEntity.status(404).body("Access token for user with ID: " + USER_ID + " does not exist");
        }
        StyngClient client = initStyngClient(accessToken.getValue());
        StyngsMyResponse response = client.my();
        return ResponseEntity.of(Optional.of(response));
    }

    public ResponseEntity<?> buy(String styngId) {

        AccessToken accessToken = authDao.findAccessTokenByUserId(USER_ID);
        if(accessToken == null){
            return ResponseEntity.status(404).body("Access token for user with ID: " + USER_ID + " does not exist");
        }
        StyngClient client = initStyngClient(accessToken.getValue());
        ProductBuyResponse response = client.buy(UUID.fromString(styngId));

        //TODO: confirm payment api
       PaymentsClient paymentsClient = Feign.builder()
                .contract(new SpringMvcContract())
                .requestInterceptor(new AuthRequestInterceptor())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .target(PaymentsClient.class, "http://localhost:8080/payments");

       //TODO: finish with exception handling
       try{
           paymentsClient.confirm(ConfirmPaymentRequest.builder()
                   .trxId(UUID.fromString("a366e90f-c1a8-41fd-b4aa-a6969b75f678"))
                   .appId(UUID.fromString("53b26ca6-5b48-4df2-bb38-0c5a4f1880f6"))
                   .billingType(BillingType.PROMO)
                   .payType("paytype")
                   .subscriptionId("1234")
                   .userIp("userIp")
                   .build());
       }catch(FeignException e){
           log.error("Exception while trying to confirm payment: " + e.getMessage());
           return ResponseEntity.status(404).body(Optional.of(e.getMessage()));
        }
       return ResponseEntity.of(Optional.of(response));
    }

    private StyngClient initStyngClient(String sdkStoreScopedToken){
        return Feign.builder()
                .contract(new SpringMvcContract())
                .requestInterceptor(new AuthRequestInterceptor(sdkStoreScopedToken))
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .target(StyngClient.class, "http://localhost:8080/styngs");
    }

}

