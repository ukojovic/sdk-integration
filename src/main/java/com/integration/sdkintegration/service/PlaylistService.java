package com.integration.sdkintegration.service;

import com.integration.sdkintegration.model.PlaylistSession;
import com.integration.sdkintegration.model.dto.PlaylistNextResponse;
import com.integration.sdkintegration.model.dto.PlaylistResponse;
import com.integration.sdkintegration.model.dto.PlaylistSessionBody;
import com.integration.sdkintegration.model.dto.PlaylistSessionResponse;
import com.integration.sdkintegration.service.dao.PlaylistDao;
import com.integration.sdkintegration.service.feign.PlaylistClient;
import feign.Feign;
import feign.FeignException;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class PlaylistService {

    private final String USER_ID = "a366e90f-c1a8-41fd-b4aa-a6969b75f676";
    private PlaylistDao playlistDao;

    public PlaylistService(PlaylistDao playlistDao){
        this.playlistDao = playlistDao;
    }

    public PlaylistResponse getAllPlaylists(String sdkToken) {

        PlaylistClient client = initPlaylistClient(sdkToken);
        return client.playlists();
    }

    public PlaylistSession startSessionForPlaylist(String id, String sdkToken) {

        PlaylistClient client = initPlaylistClient(sdkToken);
        PlaylistSessionResponse response = client.startPlaylist(id);

        PlaylistSession playlistSession = PlaylistSession.builder()
                .sessionId(response.getSessionId())
                .playlistId(id)
                .userId(USER_ID)
                .build();
        return playlistDao.saveSession(playlistSession);
    }

    public PlaylistNextResponse requestNextSong(String playlistId, String sdkToken) {

        PlaylistSession playlistSession = playlistDao.findSessionByPlaylistIdAndUserId(playlistId, USER_ID);
        PlaylistClient client = initPlaylistClient(sdkToken);
        PlaylistSessionBody playlistSessionBody = PlaylistSessionBody.builder()
                .sessionId(playlistSession.getSessionId())
                .format("AAC")
                .build();

        //TODO: Exception handling(ex. There is no more songs on playlist etc)
        //==> delete row from playlist_session table
        return client.nextSong(playlistId, playlistSessionBody);
    }

    public ResponseEntity<?> skipSong(String playlistId, String sdkToken) {

        PlaylistSession playlistSession = playlistDao.findSessionByPlaylistIdAndUserId(playlistId, USER_ID);
        PlaylistClient client = initPlaylistClient(sdkToken);
        PlaylistSessionBody playlistSessionBody = PlaylistSessionBody.builder()
                .sessionId(playlistSession.getSessionId())
                .format("AAC")
                .build();

        try{
            PlaylistNextResponse response = client.skipSong(playlistId, playlistSessionBody);
            return ResponseEntity.of(Optional.of(response));
        }catch(FeignException e){
            log.info("Feign exception while trying to skip playlist: " + e.getMessage());
            return ResponseEntity.of(Optional.of(e.getMessage()));
        }
    }

    public ResponseEntity<?> resume(String playlistId, String sdkToken) {

        PlaylistSession playlistSession = playlistDao.findSessionByPlaylistIdAndUserId(playlistId, USER_ID);
        PlaylistClient client = initPlaylistClient(sdkToken);
        PlaylistSessionBody playlistSessionBody = PlaylistSessionBody.builder()
                .sessionId(playlistSession.getSessionId())
                .format("AAC")
                .build();

        try{
            PlaylistNextResponse response = client.resume(playlistId, playlistSessionBody);
            return ResponseEntity.of(Optional.of(response.getTrackUrl()));
        }catch(FeignException e){
            log.error("Exception while trying to resume playlist: " + e.getMessage());
            return ResponseEntity.of(Optional.of(e.getMessage()));
        }
    }

    private PlaylistClient initPlaylistClient(String sdkToken){
        return Feign.builder()
                .contract(new SpringMvcContract())
                .requestInterceptor(new AuthRequestInterceptor(sdkToken))
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .target(PlaylistClient.class, "http://localhost:8080/playlists");
    }
}














