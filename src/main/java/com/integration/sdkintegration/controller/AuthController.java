package com.integration.sdkintegration.controller;


import com.integration.sdkintegration.model.RefreshTokenRequest;
import com.integration.sdkintegration.model.SDKStoreTokenRequest;
import com.integration.sdkintegration.model.TokenResponse;
import com.integration.sdkintegration.service.AuthService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    private AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @GetMapping(path = "/scope-token", produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<String> obtainSDKScopeToken() {
        return authService.obtainSDKScopeToken();
    }

    @PostMapping(path = "/store-token", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Optional<TokenResponse> obtainSDKStoreToken(@RequestBody SDKStoreTokenRequest storeTokenRequest) {
        return authService.obtainSDKStoreToken(storeTokenRequest.getSdkScopeToken());
    }

    @PostMapping(path = "/refresh-token", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Optional<TokenResponse> refreshToken(@RequestBody RefreshTokenRequest refreshTokenRequest){
        return authService.refreshToken(refreshTokenRequest.getCurrentRefreshToken());
    }
}
