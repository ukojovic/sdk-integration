package com.integration.sdkintegration.controller;

import com.integration.sdkintegration.model.PlaylistSession;
import com.integration.sdkintegration.model.dto.PlaylistNextResponse;
import com.integration.sdkintegration.model.dto.PlaylistResponse;
import com.integration.sdkintegration.service.PlaylistService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.Optional;

@RestController
@RequestMapping(value = "/playlists")
public class PlaylistController {

    private PlaylistService playlistService;

    public PlaylistController(PlaylistService playlistService){
        this.playlistService = playlistService;
    }

    @PostMapping(path = "/all")
    public PlaylistResponse getAllPlaylists(@RequestParam String sdkToken){
        return playlistService.getAllPlaylists(sdkToken);
    }

    @PostMapping(path = "/{id}/start")
    public PlaylistSession startSessionForPlaylist(@PathParam("id") String id, @RequestParam(name = "sdkToken") String sdkToken){
       return playlistService.startSessionForPlaylist(id, sdkToken);
    }

    @PostMapping(path = "/{id}/next")
    public PlaylistNextResponse requestNextSong(@PathParam("id") String id, @RequestParam(name = "sdkToken") String sdkToken){
        return playlistService.requestNextSong(id, sdkToken);
    }

    @PostMapping(path = "/{id}/skip")
    public ResponseEntity<?> skipSong(@PathParam("id") String id, @RequestParam(name = "sdkToken") String sdkToken){
        return playlistService.skipSong(id, sdkToken);
    }

    @PostMapping(path = "/{id}/resume")
    public ResponseEntity<?> resume(@PathParam("id") String id, @RequestParam(name = "sdkToken") String sdkToken){
         return playlistService.resume(id, sdkToken);
    }

}
