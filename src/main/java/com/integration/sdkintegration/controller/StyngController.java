package com.integration.sdkintegration.controller;

import com.integration.sdkintegration.service.StyngService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.websocket.server.PathParam;

@RestController
@RequestMapping(value = "/styngs")
public class StyngController {

    private StyngService styngService;

    public StyngController(StyngService styngService){
        this.styngService = styngService;
    }

    @PostMapping(path = "/all")
    public ResponseEntity<?> getAllStyngs(@RequestParam(name = "pageSize") Integer pageSize,
                                          @RequestParam(name = "pageIndex") Integer pageIndex,
                                          @RequestParam(name = "searchInput", required = false) String searchInput){
        return styngService.getAll(pageSize, pageIndex, searchInput);
    }

    @GetMapping(path = "/my")
    public ResponseEntity<?> myStyngs(){
        return styngService.myStyngs();
    }

    @PostMapping(path = "/{id}/buy")
    public ResponseEntity<?> buy(@PathParam("id")String id){
       return styngService.buy(id);
    }
}
