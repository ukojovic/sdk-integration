package com.integration.sdkintegration.repository;

import com.integration.sdkintegration.model.AccessToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccessTokenRepository extends JpaRepository<AccessToken, Integer> {

    AccessToken findByUserId(String userId);
}
