package com.integration.sdkintegration.repository;

import com.integration.sdkintegration.model.RefreshToken;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Integer> {

    RefreshToken findByUserId(String userId);
}
