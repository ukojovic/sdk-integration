package com.integration.sdkintegration.repository;

import com.integration.sdkintegration.model.PlaylistSession;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaylistSessionRepository extends JpaRepository<PlaylistSession, Integer> {

    PlaylistSession findByPlaylistIdAndUserId(String playlistId, String userId);
}
